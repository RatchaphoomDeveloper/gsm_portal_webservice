﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSM_PORTAL_WEBSERVICE.Controllers
{
    [Authorize]
    public class LoginController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        [Route("Api/Login")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
