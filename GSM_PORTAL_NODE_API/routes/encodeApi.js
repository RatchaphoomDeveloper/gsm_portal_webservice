var express = require('express');
var router = express.Router();
var Cryptr = require("cryptr")
var cryptr = new Cryptr('gsmportal')
var md5 = require('md5');

/* GET home page. */
router.get('/encodeapi/encode', function (req, res, next) {
    var encstring = cryptr.encrypt("Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.17.15.64)(PORT = 1531)))(CONNECT_DATA =(SID = TESTDB)(SERVER = DEDICATED)));User ID=GSMLDEV;Password=GSMLDEV123;")
    var decstring = cryptr.decrypt(encstring)
    var md5Conn = md5("Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.17.15.64)(PORT = 1531)))(CONNECT_DATA =(SID = TESTDB)(SERVER = DEDICATED)));User ID=GSMLDEV;Password=GSMLDEV123;")
    res.status(200).json({
        message: "success", data: {
            encstring: encstring,
            decstring: decstring,
            md5:md5Conn
        }
    })
});

module.exports = router;
