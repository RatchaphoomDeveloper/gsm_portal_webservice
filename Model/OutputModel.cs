﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class OutputModel
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }

        //Constructor
        public OutputModel() { }

        public OutputModel(int status, string message, object data)
        {
            Status = status;
            Message = message;
            Data = data;
        }
    }
}
