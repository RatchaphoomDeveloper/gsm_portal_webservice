﻿using Model.AppConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Utility
    {
        private static Utility _instance = null;

        public static Utility Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Utility();
                }

                return _instance;
            }
        }

        public JsonSerializerSettings GetJsonSettings()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                //MissingMemberHandling = MissingMemberHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss",
                Formatting = Newtonsoft.Json.Formatting.Indented
            };

            return settings;
        }

        public OutputModel GetHttpErrorResponse(WebException webExc)
        {
            var result = new OutputModel();

            try
            {
                //Get http error message
                using (var response = webExc.Response)
                {
                    if (response != null)
                    {
                        var httpResponse = (HttpWebResponse)response;
                        result.Status = (int)httpResponse.StatusCode;
                        result.Message = httpResponse.StatusDescription;
                    }
                    else
                    {
                        result.Status = SystemStatus.PROCESS_FAILED;
                        result.Message = webExc.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = SystemStatus.PROCESS_FAILED;
                result.Message = ex.Message;
            }

            return result;
        }
        string key = "Tm2%Xy5!P1a@K7C01";
        public string MD5Decryption(string cipher)
        {
            try
            {
                using (var md5 = new MD5CryptoServiceProvider())
                {
                    using (var tdes = new TripleDESCryptoServiceProvider())
                    {
                        tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                        tdes.Mode = CipherMode.ECB;
                        tdes.Padding = PaddingMode.PKCS7;

                        using (var transform = tdes.CreateDecryptor())
                        {
                            byte[] cipherBytes = Convert.FromBase64String(cipher);
                            byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                            return UTF8Encoding.UTF8.GetString(bytes);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Utity/MD5Decryption : " + ex.Message);
            }
        }
    }
}
