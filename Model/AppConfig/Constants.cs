﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.AppConfig
{
    public class SystemStatus
    {
        public const int SUCCESS = 1;
        public const int ERROR = -5;
        public const int DATA_NOT_FOUND = -10;
        public const int INVALID_USER = -20;
        public const int PROCESS_FAILED = -30;
        //public const int SERVER_NOT_FOUND = 404;
        //public const int SERVER_ERROR = 500;
    }
}
