﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.AppConfig
{
    public class Messenger
    {
        private static Messenger _instance = null;
        public static Messenger Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Messenger();
                }

                return _instance;
            }
        }

        public string GetMessage(int rescode, string value = "")
        {
            try
            {
                if (rescode == SystemStatus.SUCCESS) return "ทำรายการสำเร็จ";
                if (rescode == SystemStatus.PROCESS_FAILED) return "ไม่สำเร็จ กรุณาทำรายการใหม่หรือติดต่อผู้ดูแลระบบ";
                if (rescode == SystemStatus.DATA_NOT_FOUND) return "ไม่พบข้อมูล";
                if (rescode == SystemStatus.INVALID_USER) return "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง";

                return "ไม่พบคำอธิบายสำหรับข้อผิดพลาดนี้ กรุณาทำรายการใหม่หรือติดต่อผู้ดูแลระบบ";
            }
            catch (Exception ex)
            {
                return "GetMessage : " + ex.Message;
            }
        }
    }
}