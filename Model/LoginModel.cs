﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    #region LoginAppFingerModels
    public class LoginAppFingerModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    #endregion



    public class LoginMobileResponseModel
    {
        public List<LoginData> UserData { get; set; }

    }

    public class LoginData
    {
       [JsonProperty("ID")]
       public string ID { get; set; }
       [JsonProperty("NAME")]
       public string NAME { get; set; }
       [JsonProperty("LASTNAME")]
       public string LASTNAME { get; set; }
    }

}
