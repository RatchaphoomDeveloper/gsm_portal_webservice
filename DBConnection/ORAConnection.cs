﻿using Model.AppConfig;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace DBConnection
{
    public class ORAConnection
    {
        #region Singleton
        private static ORAConnection _instance = null;

        public static ORAConnection Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ORAConnection();
                }

                return _instance;
            }
        }
        #endregion

        #region DBConnect
        public OracleConnection DBConnect
        {
            get
            {
                try
                {
                    OracleConnection oraConn = null;
                    string connStr = AppSettings.Ins.GetOracleConnStr();

                    oraConn = new OracleConnection(connStr);
                    return oraConn;
                }
                catch (Exception ex)
                {
                    throw new Exception("ORAConnection/DBConnection : " + ex.Message);
                }
            }
        }
        #endregion

        #region DBCloseConnect
        public void DBCloseConnect(OracleConnection oraConn)
        {
            try
            {
                if (oraConn != null)
                {
                    if (oraConn.State == ConnectionState.Open)
                    {
                        oraConn.Close();
                    }

                    oraConn.Dispose();
                }
            }
            catch (Exception)
            {
                //Task.Run(() => SysModel.Instance.WriteToLog("", "ORConnection/DBCloseConnect", ex.Message, ex));
            }
        }
        #endregion
    }
}
